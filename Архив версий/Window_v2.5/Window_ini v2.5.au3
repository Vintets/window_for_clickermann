#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=icon\window_list3.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Comment=Window
#AutoIt3Wrapper_Res_Fileversion=2.5
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#AutoIt3Wrapper_Res_requestedExecutionLevel=None
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region    ************ Includes ************
#include <Array.au3>
#include <WindowsConstants.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <FontConstants.au3>
#include <StaticConstants.au3>
#include <WinAPI.au3>
#EndRegion ************ Includes ************


Global $section, $result

FileInstall("Window.ini", @ScriptDir & "\Window.ini")

If @Compiled Then
    ; ������ �����������, ���� ��� �����-������ ���������� ��������� ������
    If $CmdLine[0] = 0 Then Exit(1)
    $section = $CmdLine[1]
Else
    ;$section = 'Window'
    ;$section = 'Window_select'
    ;$section = 'Window_number'
    $section = 'Window_checkbox'
    ;$section = 'Window_checkbox_flags'
EndIf

;MsgBox (0,"���������",$section,0)
$flag = IniRead ('Window.ini', $section, 'flag', '0' )

Select
    Case $section = 'Window_select' And ($flag = 1 Or $flag = 10)
        $result = Window_select1($section)
    Case $section = 'Window_select' And ($flag = 2 Or $flag = 20)
        $result = Window_select2($section)
    Case $section = 'Window'
        $result = Window($section)
    Case $section = 'Window_number'
        $result = Window_number($section)
    Case $section = 'Window_checkbox' And ($flag = 1 Or $flag = 10)
        $result = Window_checkbox($section)
    Case $section = 'Window_checkbox_flags'
        $result = Window_checkbox($section)
    Case Else
        Exit
EndSelect
IniWrite ('Window.ini', $section, 'return', $result)
Exit


Func Window_checkbox($section)
    Local $flag, $title, $text, $timeout, $Lmax = '', $result = -1, $hWin = 126, $aPunct[61][2], $hTimerWait
    Local $podstrmax = 0, $sdvig = 0, $Allsdvig = 0, $gold = Int(@DesktopWidth*0.618), $h1 = 20, $h2 = 16, $h3 = 14
    Local $hGUI, $winX, $winY, $hCheckbox[61], $iFont_Size = 8.3, $iFont_Weight = -1, $sFont_Attrib = -1, $sFont_Name = 'MS Sans Serif'
    Local $hButtonOk, $hButtonCancel, $Ok_X, $Cancel_X
    Local $tFlags, $outFlags = -1, $check

    $aPunct[0][1] = 0
    $gold = 793

    ; $aPunct[0][0] - ����� �������
    ; $aPunct[0][1] - ����� ��������
    ; $aPunct[1][0] - ���������� �������� � ������
    ; $aPunct[1][1] - ���� ����� ������

    $flag = IniRead ('Window.ini', $section, 'flag', '0')
    $title = IniRead ('Window.ini', $section, 'title', '���������')
    $text = IniRead ('Window.ini', $section, 'text', '77')
    $timeout = IniRead ('Window.ini', $section, 'timeout', '3')
    If $section = 'Window_checkbox_flags' Then
        $tFlags = IniRead ('Window.ini', $section, 'flags', '')
    EndIf
    ;MsgBox (0,"$text ", $text, 0)

    If $flag = 10 Then
        $iFont_Weight = 800
        $h1 = 22
        $h2 = 24
        $h3 = 16
    EndIf

    Local $aArray = StringSplit ($text, '|', 1)
    $aPunct[0][0] = $aArray[0]
    For $n = 1 To $aArray[0]
        $aArray2 = StringSplit ($aArray[$n], '\n', 1)
        If $aArray2[0] > $podstrmax Then $podstrmax = $aArray2[0]
        $aPunct[0][1] += $aArray2[0]
        $aPunct[$n][0] = $aArray2[0]
        $aPunct[$n][1] = $aArray[$n]
        For $i = 1 To $aArray2[0]
            If StringLen($aArray2[$i]) > StringLen($Lmax) Then $Lmax = $aArray2[$i]
        Next
    Next

    If $section = 'Window_checkbox_flags' Then
        Local $aFlags = StringSplit ($tFlags, ',', 1)
        If UBound($aArray) <> UBound($aFlags) Then
            MsgBox (4112,'������ ����������', '��������� ��������� ������ �������')
            ;ConsoleWrite($result & @CRLF)
            Return $result
        EndIf
    EndIf

    $aData_Width = __GUICtrlTFLabel_GetTextSize($Lmax, $iFont_Size, $iFont_Weight, $sFont_Attrib, $sFont_Name)
    ; _ArrayDisplay( $aPunct, "��� ������" )
    ; MsgBox (0,'����� ������� ������', '����� ������� ������ ' & StringLen($Lmax)& ' ��������' & @CRLF & '����� ������: ' & $aData_Width[0] & ' ����.' & @CRLF & "������: " & $aData_Width[1]& @CRLF & $Lmax, 0)
    ; MsgBox (0,'�������� ��������', '�������� �������� ' & $podstrmax, 0)

    For $n = 1 To $aPunct[0][0]
        $Allsdvig += $aPunct[$n][0]-1
    Next

    If $aData_Width[0] < 188 Then
        $winX = 188
    ElseIf $aData_Width[0] + 60 +17 <= $gold Then
        $winX = $aData_Width[0] + 60 + 17
    Else
        $winX = $gold
    EndIf
    If $winX > 260 Then
        $Ok_X = 60
        $Cancel_X = $winX - 120
    Else
        $Ok_X = 20
        $Cancel_X = $winX - 80
    EndIf
    $winY = 18+($h1+$h3)*$aPunct[0][0] + $h3*$Allsdvig + 30

    $hGUI = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_CAPTION, $WS_SYSMENU, $WS_POPUP), $WS_EX_TOPMOST)
    $hButtonOk = GUICtrlCreateButton('Ok', $Ok_X, $winY-30, 60, 25, $BS_CENTER)
    $hButtonCancel = GUICtrlCreateButton('������', $Cancel_X, $winY-30, 60, 25, $BS_CENTER)
    For $n = 1 To $aPunct[0][0]
        $hCheckbox[$n] = GUICtrlCreateCheckbox('', 17, 14+($h1+$h3)*($n-1) + $sdvig, 20, 20, BitOR($GUI_SS_DEFAULT_CHECKBOX, $WS_TABSTOP))
        ;GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
        If $section = 'Window_checkbox_flags' and $aFlags[$n] = 1 Then
            GUICtrlSetState($hCheckbox[$n], $GUI_CHECKED)
        EndIf

        $aArray2 = StringSplit ($aPunct[$n][1], '\n', 1)
        For $p = 1 To $aPunct[$n][0] ; $aArray2[0]
            $temp = GUICtrlCreateLabel($aArray2[$p], 40, 18+($h1+$h3)*($n-1) + $h3*($p-1) + $sdvig, $winX - 57)
            ; GUICtrlSetBkColor(-1, 0xf5cccc)
            If $flag = 10 Then GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
        Next
        $sdvig += $h3*($aPunct[$n][0]-1)
    Next

    GUISetState(@SW_SHOW, $hGUI)

    $hTimerWait = TimerInit()
    While 1
        If $timeout <> 0 And TimerDiff($hTimerWait) > $timeout*1000 Then ExitLoop

        $nMsg = GUIGetMsg()
        Select
            Case $nMsg = $GUI_EVENT_CLOSE Or $nMsg = $hButtonCancel
                ExitLoop
            Case $nMsg = $hButtonOk
                For $n = 1 To $aPunct[0][0]
                    If $section = 'Window_checkbox' Then
                        If BitAnd(GUICtrlRead($hCheckbox[$n]), $GUI_CHECKED) Then
                            If $result = -1 Then
                                $result = '' & $n
                            Else
                                $result &= ',' & $n
                            EndIf
                        EndIf
                    ElseIf $section = 'Window_checkbox_flags' Then
                        $result = 1
                        If BitAnd(GUICtrlRead($hCheckbox[$n]), $GUI_CHECKED) Then
                            $check = 1
                        Else
                            $check = 0
                        EndIf
                        If $outFlags = -1 Then
                            $outFlags = '' & $check
                        Else
                            $outFlags &= ',' & $check
                        EndIf
                    EndIf
                Next
                If $section = 'Window_checkbox_flags' Then
                    IniWrite ('Window.ini', $section, 'flags', $outFlags)
                EndIf
                ;ConsoleWrite($result)
                ExitLoop
        EndSelect
    WEnd
    Return $result
EndFunc

Func Window_number($section)
    Local $title, $text, $count, $timeout, $result = -1, $aArray, $text_out, $row_text, $num = 0, $hTimerWait
    Local $hGUI, $htext, $winX, $winY, $winX = 585, $row, $col
    ;Local $Lmax = '', $iFont_Size = 8.3, $iFont_Weight = -1, $sFont_Attrib = -1, $sFont_Name = 'MS Sans Serif'

    $title = IniRead ('Window.ini', $section, 'title', '���������')
    $text = IniRead ('Window.ini', $section, 'text', '')
    $count = IniRead ('Window.ini', $section, 'count', '0')
    $timeout = IniRead ('Window.ini', $section, 'timeout', '3')
    ;If $count = 0 Then Return $result
    Local $hButton[$count]

    $aArray = StringSplit ($text, '\n', 1)
    $text_out = $aArray[1]
    ;$Lmax = $aArray[1]
    For $n = 2 To $aArray[0]
        $text_out &= @LF & $aArray[$n]
        ;If StringLen($aArray[$n]) > StringLen($Lmax) Then $Lmax = $aArray[$n]
    Next
    ;$aData_Width = __GUICtrlTFLabel_GetTextSize($Lmax, $iFont_Size, $iFont_Weight, $sFont_Attrib, $sFont_Name)


    Switch $count
        Case 0
            Return $result
        Case 1 To 5
            $row = 1
            $col = $count
        Case 6 To 10
            $row = 2
            $col = 5
        Case Else
            If ($count/10) - Int($count/10) = 0 Then
                $row = Int($count/10)
            Else
                $row = Int($count/10) + 1
            EndIf
            ;MsgBox(4096, "", "$row " & $row, 3)
            $col = 10
    EndSwitch

    If $text = '' Then
        $row_text = 0
    Else
        $row_text = $aArray[0]
    EndIf
    $winY = 125 + $row_text * 20 + ($row-1)*50

    $hGUI = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_CAPTION, $WS_SYSMENU, $WS_POPUP), $WS_EX_TOPMOST)

    If $row_text > 0 Then
        $htext = GUICtrlCreateLabel($text_out, 50, 32, 485, $row_text * 20, $SS_CENTER)
    EndIf


    For $r = 0 To $row-1
        For $c = 0 To $col-1
            $num += 1
            If $num > $count Then ExitLoop(2)
            ;MsgBox(4096, "", "����� " & $num-1, 1)
            $hButton[$num-1] = GUICtrlCreateButton($num, 550/($col+1) * ($c+1), (50 + $row_text*20) + $r*50, 35, 25, BitOR($BS_CENTER, $BS_VCENTER))
        Next
    Next

    GUISetState(@SW_SHOW, $hGUI)

    $hTimerWait = TimerInit()
    While 1
        If $timeout <> 0 And TimerDiff($hTimerWait) > $timeout*1000 Then ExitLoop

        $nMsg = GUIGetMsg()
        If $nMsg = $GUI_EVENT_CLOSE Then ExitLoop
        For $n = 1 To $count
            If $nMsg = $hButton[$n-1] Then
                $result = $n
                ExitLoop(2)
            EndIf
        Next
    WEnd
    Return $result
EndFunc

Func Window_select1($section)
    Local $flag, $title, $text, $timeout, $Lmax = '', $result = -1, $hWin = 126, $aPunct[61][2], $hTimerWait
    Local $podstrmax = 0, $sdvig = 0, $Allsdvig = 0, $gold = Int(@DesktopWidth*0.618), $h1 = 20, $h2 = 16, $h3 = 14
    Local $hGUI, $winX, $winY, $hButton[61], $iFont_Size = 8.3, $iFont_Weight = -1, $sFont_Attrib = -1, $sFont_Name = 'MS Sans Serif'

    $aPunct[0][1] = 0
    $gold = 793

    ; $aPunct[0][0] - ����� �������
    ; $aPunct[0][1] - ����� ��������
    ; $aPunct[1][0] - ���������� �������� � ������
    ; $aPunct[1][1] - ���� ����� ������

    $flag = IniRead ('Window.ini', $section, 'flag', '0')
    $title = IniRead ('Window.ini', $section, 'title', '���������')
    $text = IniRead ('Window.ini', $section, 'text', '77')
    $timeout = IniRead ('Window.ini', $section, 'timeout', '3')
    ;MsgBox (0,"$text ", $text, 0)

    If $flag = 10 Then
        $iFont_Weight = 800
        $h1 = 22
        $h2 = 24
        $h3 = 16
    EndIf

    $aArray = StringSplit ($text, '|', 1)
    $aPunct[0][0] = $aArray[0]
    For $n = 1 To $aArray[0]
        $aArray2 = StringSplit ($aArray[$n], '\n', 1)
        If $aArray2[0] > $podstrmax Then $podstrmax = $aArray2[0]
        $aPunct[0][1] += $aArray2[0]
        $aPunct[$n][0] = $aArray2[0]
        $aPunct[$n][1] = $aArray[$n]
        For $i = 1 To $aArray2[0]
            If StringLen($aArray2[$i]) > StringLen($Lmax) Then $Lmax = $aArray2[$i]
        Next
    Next

    $aData_Width = __GUICtrlTFLabel_GetTextSize($Lmax, $iFont_Size, $iFont_Weight, $sFont_Attrib, $sFont_Name)
    ; _ArrayDisplay( $aPunct, "��� ������" )
    ; MsgBox (0,'����� ������� ������', '����� ������� ������ ' & StringLen($Lmax)& ' ��������' & @CRLF & '����� ������: ' & $aData_Width[0] & ' ����.' & @CRLF & "������: " & $aData_Width[1]& @CRLF & $Lmax, 0)
    ; MsgBox (0,'�������� ��������', '�������� �������� ' & $podstrmax, 0)

    For $n = 1 To $aPunct[0][0]
        $Allsdvig += $aPunct[$n][0]-1
    Next

    If $aData_Width[0] < 188 Then
        $winX = 188
    ElseIf $aData_Width[0] + 60 +17 <= $gold Then
        $winX = $aData_Width[0] + 60 + 17
    Else
        $winX = $gold
    EndIf
    $winY = 18+($h1+$h3)*$aPunct[0][0] + $h3*$Allsdvig

    $hGUI = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_CAPTION, $WS_SYSMENU, $WS_POPUP), $WS_EX_TOPMOST)
    For $n = 1 To $aPunct[0][0]
        $hButton[$n] = GUICtrlCreateButton($n, 17, 14+($h1+$h3)*($n-1) + $sdvig, 30, 20, $BS_CENTER)
        GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")

        $aArray2 = StringSplit ($aPunct[$n][1], '\n', 1)
        For $p = 1 To $aPunct[$n][0] ; $aArray2[0]
            $temp = GUICtrlCreateLabel($aArray2[$p], 60, 18+($h1+$h3)*($n-1) + $h3*($p-1) + $sdvig, $winX - 77)
            ; GUICtrlSetBkColor(-1, 0xf5cccc)
            If $flag = 10 Then GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
        Next
        $sdvig += $h3*($aPunct[$n][0]-1)
    Next

    GUISetState(@SW_SHOW, $hGUI)

    $hTimerWait = TimerInit()
    While 1
        If $timeout <> 0 And TimerDiff($hTimerWait) > $timeout*1000 Then ExitLoop

        $nMsg = GUIGetMsg()
        If $nMsg = $GUI_EVENT_CLOSE Then ExitLoop
        For $n = 1 To $aPunct[0][0]
            If $nMsg = $hButton[$n] Then
                $result = $n
                ExitLoop(2)
            EndIf
        Next
    WEnd
    Return $result
EndFunc

Func Window_select2($section)
    Local $flag, $title, $text, $timeout, $Lmax = '', $result = -1, $hWin = 126, $aPunct[61][2], $hTimerWait
    Local $podstrmax = 0, $text_out, $sdvig = 0, $Allsdvig = 0, $gold = Int(@DesktopWidth*0.618), $h1 = 20, $h2 = 16, $h3 = 14
    Local $hGUI, $winX, $winY, $hButton[61], $iFont_Size = 8.3, $iFont_Weight = -1, $sFont_Attrib = -1, $sFont_Name = 'MS Sans Serif'

    $aPunct[0][1] = 0
    $gold = 793

    ; $aPunct[0][0] - ����� �������
    ; $aPunct[0][1] - ����� ��������
    ; $aPunct[1][0] - ���������� �������� � ������
    ; $aPunct[1][1] - ���� ����� ������

    $flag = IniRead ('Window.ini', $section, 'flag', '0')
    $title = IniRead ('Window.ini', $section, 'title', '���������')
    $text = IniRead ('Window.ini', $section, 'text', '77')
    $timeout = IniRead ('Window.ini', $section, 'timeout', '3')
    ;MsgBox (0,"$text ", $text, 0)

    If $flag = 20 Then
        $iFont_Weight = 800
        $h1 = 22
        $h2 = 24
        $h3 = 16
    EndIf

    $aArray = StringSplit ($text, '|', 1)
    $aPunct[0][0] = $aArray[0]
    For $n = 1 To $aArray[0]
        $aArray2 = StringSplit ($aArray[$n], '\n', 1)
        If $aArray2[0] > $podstrmax Then $podstrmax = $aArray2[0]
        $aPunct[0][1] += $aArray2[0]
        $aPunct[$n][0] = $aArray2[0]
        $aPunct[$n][1] = $aArray[$n]
        For $i = 1 To $aArray2[0]
            If StringLen($aArray2[$i]) > StringLen($Lmax) Then $Lmax = $aArray2[$i]
        Next
    Next

    $aData_Width = __GUICtrlTFLabel_GetTextSize($Lmax, $iFont_Size, $iFont_Weight, $sFont_Attrib, $sFont_Name)
    ; _ArrayDisplay( $aPunct, "��� ������" )
    ; MsgBox (0,'����� ������� ������', '����� ������� ������ ' & StringLen($Lmax)& ' ��������' & @CRLF & '����� ������: ' & $aData_Width[0] & ' ����.' & @CRLF & "������: " & $aData_Width[1]& @CRLF & $Lmax, 0)
    ; MsgBox (0,'�������� ��������', '�������� �������� ' & $podstrmax, 0)

    For $n = 1 To $aPunct[0][0]
        $Allsdvig += $aPunct[$n][0]-1
    Next

    If $aData_Width[0] < 188 Then
        $winX = 188
    ElseIf $aData_Width[0] + 60 +17 <= $gold Then
        $winX = $aData_Width[0] + 60 + 17
    Else
        $winX = $gold
    EndIf
    $winY = 18+($h1+$h3)*$aPunct[0][0] + $h2*$Allsdvig + 10

    $hGUI = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_CAPTION, $WS_SYSMENU, $WS_POPUP), $WS_EX_TOPMOST)
    For $n = 1 To $aPunct[0][0]
        $aArray2 = StringSplit ($aPunct[$n][1], '\n', 1)
        $text_out = $aArray2[1]
        For $p = 2 To $aPunct[$n][0] ; $aArray2[0]
            $text_out &= @CRLF & $aArray2[$p]
        Next

        $hButton[$n] = GUICtrlCreateButton($text_out, 17, 18+($h1+$h3)*($n-1) + $sdvig, $winX-34, $h2*$aPunct[$n][0] + 10, $BS_MULTILINE)
        If $flag = 20 Then GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
        $sdvig += $h2*($aPunct[$n][0]-1)
    Next

    GUISetState(@SW_SHOW, $hGUI)

    $hTimerWait = TimerInit()
    While 1
        If $timeout <> 0 And TimerDiff($hTimerWait) > $timeout*1000 Then ExitLoop

        $nMsg = GUIGetMsg()
        If $nMsg = $GUI_EVENT_CLOSE Then ExitLoop
        For $n = 1 To $aPunct[0][0]
            If $nMsg = $hButton[$n] Then
                $result = $n
                ExitLoop(2)
            EndIf
        Next
    WEnd
    Return $result
EndFunc

Func Window($section)
    Local $flag, $title, $text, $timeout, $text_out, $result

    $flag = IniRead ('Window.ini', $section, 'flag', '0' )
    $title = IniRead ('Window.ini', $section, 'title', '���������' )
    $text = IniRead ('Window.ini', $section, 'text', '' )
    $timeout = IniRead ('Window.ini', $section, 'timeout', '3' )

    $aArray = StringSplit ($text, '\n', 1)
    $text_out = $aArray[1]
    For $n = 2 To $aArray[0]
        $text_out &= @LF & $aArray[$n]
    Next
    $result = MsgBox ($flag,$title,$text_out,$timeout)
    Return $result
EndFunc

; #HELPER FUNCTION - Get text size for the label control#
Func __GUICtrlTFLabel_GetTextSize($sData, $iFontSize = 8.5, $iFontWeight = $FW_NORMAL, $iFontAttribs = 0, $sFontName = 'Arial', $iFixTabs = 0)
    Local $sTabs, $sAdd, $bFItalic, $bFUnderline, $bFStrikeout, $h_GDW_GUI, $hDC, $intDeviceCap, $intFontHeight, $hFont, $tSIZE, $aRet

    If $iFixTabs Then
        $sData = StringReplace($sData, @TAB, '     ')

        If @extended Then
            For $i = 1 To @extended + 1
                $sTabs &= '    ' & $sAdd
                $sAdd &= ' '
            Next
        EndIf

        $sData &= $sTabs
    EndIf

    $bFItalic = BitAND($iFontAttribs, 2) = 2
    $bFUnderline = BitAND($iFontAttribs, 4) = 4
    $bFStrikeout = BitAND($iFontAttribs, 8) = 8

    $h_GDW_GUI = GUICreate("Get Data Width", 10, 10, -100, -100, 0x80880000, 0x00000080)
    $hDC = _WinAPI_GetDC($h_GDW_GUI)
    $intDeviceCap = _WinAPI_GetDeviceCaps($hDC, $LOGPIXELSY)
    $intFontHeight = _WinAPI_MulDiv($iFontSize, $intDeviceCap, 72)
    ;$hFont = _SendMessage($h_GDW_GUI, $WM_GETFONT) ; ���������� ���������� ������, ������������� ��� �������� ������.
    $hFont = _WinAPI_CreateFont(-$intFontHeight, 0, 0, 0, $iFontWeight, $bFItalic, $bFUnderline, $bFStrikeout, $DEFAULT_CHARSET, $OUT_CHARACTER_PRECIS, $CLIP_DEFAULT_PRECIS, $PROOF_QUALITY, $FIXED_PITCH, $sFontName)
    _WinAPI_SelectObject($hDC, $hFont)
    $tSIZE = _WinAPI_GetTextExtentPoint32($hDC, $sData)

    Dim $aRet[2] = [DllStructGetData($tSIZE, 'X'), DllStructGetData($tSIZE, 'Y')]

    GUIDelete($h_GDW_GUI)
    Return $aRet
EndFunc
